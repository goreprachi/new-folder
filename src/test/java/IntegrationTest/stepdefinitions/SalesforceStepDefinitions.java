package IntegrationTest.stepdefinitions;

import ch.qos.logback.classic.util.LogbackMDCAdapter;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import io.cucumber.datatable.DataTable;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;
import org.assertj.core.api.Condition;
import org.jruby.RubyProcess;
import org.junit.After;
import org.junit.Assert;
import org.junit.Assume;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import IntegrationTest.navigation.NavigateTo;

import java.util.List;
import java.util.Map;

import static java.lang.Thread.sleep;

import static org.assertj.core.api.Assertions.assertThat;

public class SalesforceStepDefinitions {

    @Steps
    NavigateTo navigateTo;

    @Given("I have login to gmail account")
    public void gmaillogin(){
        navigateTo.logintogmail();
    }

    @When("I click on compose button")
    public void iClickOnComposeButton() {
        navigateTo.composebutton();
    }

    @And("I provide (.*), (.*) and (.*)")
    public void iProvideEmailaddressBodyAndSubject(String fieldemailaddress,String fieldbody, String fieldsubject,DataTable dataTable) {
        List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
        String email = data.get(0).get("emailaddress");
        System.out.print("Email:"+email);
        String body = data.get(0).get("body");
        System.out.print("body:"+body);
        String subject= data.get(0).get("subject");
        System.out.print("subject:"+subject);
        navigateTo.Detailsofemail(fieldemailaddress,fieldbody,fieldsubject,email,body,subject);
    }

    @And("I clicks on (.*) button")
    public void iClicksOnSendButton(String send) {
        navigateTo.clickonsendbutton(send);
    }

    String M1=null;
    String M2=null;


    public void waitfortheelement() {
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



}



