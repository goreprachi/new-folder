package IntegrationTest.navigation;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.logging.Logger;

import static java.lang.Thread.sleep;

public class NavigateTo {


    SalesforcePage salesforcePage;
    //SalesforcePage salesforcePage;

    private static Logger LOGGER = Logger.getLogger("InfoLogging");


    public String getConfigurationDetails(String nameURL) {
        EnvironmentVariables props = Injectors.getInjector().getInstance(EnvironmentVariables.class);
        String configDetails = EnvironmentSpecificConfiguration.from(props).getProperty(nameURL);
        return configDetails;
    }

    public void logintogmail() {
        String Gmail= getConfigurationDetails("webdriver.base.url");
        String userName = getConfigurationDetails("Salesforce.username");
        String passWord = getConfigurationDetails("Salesforce.password");
        salesforcePage.getDriver().get(Gmail);
        salesforcePage.getDriver().manage().window().maximize();
        waitfortheelement();
        salesforcePage.userName.sendKeys(userName);
        waitfortheelement();
        salesforcePage.clickLogin.click();
        waitfortheelement();
        salesforcePage.passWord.sendKeys(passWord);
        waitfortheelement();
        salesforcePage.clickLogin.click();
        waitfortheelement();
        waitfortheelement();
    }

    public void composebutton() {
        waitfortheelement();
        waitfortheelement();
        try{
            waitfortheelement();
        salesforcePage.getDriver().findElement(By.xpath("//div[@class=\"Ls77Lb aZ6\"]//div//div//div//div//div//div[@role=\"button\"]")).click();
        waitfortheelement();}
        catch(Exception e){
        System.out.print(e);
    }}

    public void Detailsofemail(String fieldemailadress, String fieldbody,String fieldsubject,String email,String body,String subject) {

        waitfortheelement();
        //Email
        salesforcePage.getDriver().findElement(By.xpath("//input[@class=\"wA\"]//..//textarea[@name=\"to\"] ")).sendKeys(email);
        waitfortheelement();

        //Body
        salesforcePage.getDriver().findElement(By.xpath("//div[@aria-label=\"Message Body\" and @role=\"textbox\"]")).sendKeys(body);
        waitfortheelement();

        //Subject
        salesforcePage.getDriver().findElement(By.xpath("//input[@name=\"subjectbox\"]")).click();
        salesforcePage.getDriver().findElement(By.xpath("//input[@name=\"subjectbox\"]")).sendKeys(subject);
        waitfortheelement();
    }

    public void clickonsendbutton(String send) {

        salesforcePage.getDriver().findElement(By.xpath("//div[text()=\"Send\"]")).click();
        waitfortheelement();
        waitfortheelement();
    }



       public void waitfortheelement() {
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }





}





