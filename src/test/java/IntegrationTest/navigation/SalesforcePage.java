package IntegrationTest.navigation;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SalesforcePage extends PageObject {
    @FindBy(xpath = "//input[@autocomplete=\"username\"]")
    public WebElement userName;

    @FindBy(xpath = "//input[@name=\"password\"]")
    public WebElement passWord;


    @FindBy(xpath = "//div[@class=\"VfPpkd-RLmnJb\"]")
    public WebElement clickLogin;


 /*   @FindBy(xpath = "//div//span[text()=\"Next\"]")
    public WebElement nextbutton;*/

   /* @FindBy(xpath = "//div[contains(@class,'menuItemsWrapper')]//span")
    public WebElement clickOnElement;*/

    public SalesforcePage(WebDriver driver) { super(driver);
    }
}
